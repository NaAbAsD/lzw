#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "decode.h"
#include "byte.h"
#include "unistd.h"

static dico_t dico;

FILE *fdNewBook;
FILE *fileIn;
struct options *goptions;

void finMessage(int *couple) {
	free(couple);
	fflush(fdNewBook);
	resetDico(&dico);
	if (goptions->pfd != NULL && close(goptions->pfd[0]) == -1) erreur("Impossible de fermer le pipe");
	if (goptions->mode == 2) fclose(fileIn);
	fclose(fdNewBook);
	if (goptions->mode == MODE_ALL) free(goptions->pfd);
	free(goptions);
	exit(0);
}

int * decoder_triplet(byte_t triplet[3]) {
	int *decoded = (int *)calloc(2, sizeof(int));
	decoded[0] = (triplet[0] << 4) | (lire_quartetSup(triplet[1]) >> 4);
	decoded[1] = triplet[2] | (lire_quartetInf(triplet[1]) << 8);
	return decoded;
}

void decoder_couple(int couple[2]) {
	int lgth, indRecherche;
	static char S[BUFSIZ] = "\0";
	char m;
	char *complexe;

	for (int i = 0; i < 2; i++) {
		if (couple[i] <= 255) { // C'est un caractere ASCII
			if (couple[i] == 0) { // C'est un caractere null qu'on a send en buffer (cf. encode.c)
				finMessage(couple);
			}
			m = couple[i];
			fprintf(fdNewBook, "%c", couple[i]);
			// Construction de S+m
			lgth = strlen(S);
			S[lgth] = m;
			S[lgth+1] = '\0';
			// Recherche de S+m dans le dico
			for (indRecherche = 0; indRecherche < dico.nb_mots && strcmp(S, dico.mots[indRecherche]) != 0; indRecherche++);
			// Resultat
			if (indRecherche == dico.nb_mots && lgth != 0) {
				dico.mots[dico.nb_mots] = (char *)malloc((lgth + 2) * sizeof(char));
				strcpy(dico.mots[dico.nb_mots], S);
				S[0] = m; // On reinitialise avec m
				S[1] = '\0';
				dico.nb_mots++;
				if (dico.nb_mots == MAX_DICT_SIZE) { // WARNING: Ici on perd une allocation de string au dessus
					resetDico(&dico);
				}
			}
		} else { // C'est forcement un mot du dictionnaire d'avant donc mot compose
			// Construction de S+m
			if (couple[i] - 256 >= dico.nb_mots) { // Cas qui se mord la queue (Exemple : Lecture du double espace)
				lgth = strlen(S);
				S[lgth] = S[0];
				S[lgth+1] = '\0';
			} else { // Cas classique
				complexe = dico.mots[couple[i] - 256];
				lgth = strlen(S);
				strcat(S, complexe);
			}
			// Recherche de S+m dans le dico
			for (indRecherche = 0; indRecherche < dico.nb_mots && strcmp(S, dico.mots[indRecherche]) != 0; indRecherche++);
			// Resultat
			if (couple[i] - 256 < dico.nb_mots) { // Cas classique
				S[lgth] = complexe[0];
				S[lgth+1] = '\0';
			}
			if (indRecherche == dico.nb_mots && lgth != 0) {
				dico.mots[dico.nb_mots] = (char *)malloc((lgth + 2) * sizeof(char));
				strcpy(dico.mots[dico.nb_mots], S);
				if (couple[i] - 256 < dico.nb_mots) { // Cas classique
					S[0] = '\0'; // Reset de chaine
					strcat(S, complexe);
				}
				dico.nb_mots++;
				if (dico.nb_mots == MAX_DICT_SIZE) { // WARNING: Ici on perd une allocation de string au dessus
					resetDico(&dico);
				}
			}
			fprintf(fdNewBook, "%s", dico.mots[couple[i] - 256]);
		}
	}
	free(couple); // A ne pas oublier
}

void recevoir(const byte_t oc) {
	static int cpt = 0;
	static byte_t triplet[3];
	cpt += 1;
	if (cpt == 3) {
		cpt = 0;
		triplet[2] = oc;
		int *couple = decoder_triplet(triplet);
		decoder_couple(couple);
	} else {
		triplet[cpt-1] = oc;
	}
}

void decoder(struct options *options) {
	goptions = options;
	if (options->pfd != NULL && close(options->pfd[1]) == -1) erreur("Impossible de fermer le pipe");

	fdNewBook = fopen("res/decoded.txt", "w");

	if (options->mode == MODE_ALL) {
		byte_t code1, code2, code3;
		while (1) { // La sortie de boucle se fait apres la lecture d'un caractere 0 en decompression.
			if (read(options->pfd[0], &code1, sizeof(byte_t)) == -1) erreur("read code1");
			if (read(options->pfd[0], &code2, sizeof(byte_t)) == -1) erreur("read code2");
			if (read(options->pfd[0], &code3, sizeof(byte_t)) == -1) erreur("read code3");
			recevoir(code1);
			recevoir(code2);
			recevoir(code3);
		}
	} else { // Forcement mode decode donc on a un fichier bin a ouvrir
		byte_t code;
		fileIn = fopen(options->fileIn, "rb");
		while (1) { // La sortie de boucle se fait après la lecture d'un caractere 0 en decompression.
			fread(&code, 1, 1, fileIn);
			recevoir(code);
		}
	}
}