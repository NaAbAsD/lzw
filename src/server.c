#include "stdlib.h"
#include "server.h"
#include "encode.h"
#include "wait.h"

void server(struct options *options) {
	encoder(options);
	if (wait(NULL) == -1) erreur("wait");
}
