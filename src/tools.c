#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "getopt.h"
#include "ctype.h"
#include "unistd.h"
#include "sys/stat.h"
#include "wait.h"
#include "client.h"
#include "server.h"
#include "tools.h"

int getModeFromArgument(char *optarg) {
	for (int i = 0; optarg[i]; i++)
		optarg[i] = tolower(optarg[i]);
	
	if (strcmp(optarg, "all") == 0 || strcmp(optarg, "a") == 0)
		return MODE_ALL;
	else if (strcmp(optarg, "encode") == 0  || strcmp(optarg, "e") == 0)
		return MODE_ENCODE_ONLY;
	else if (strcmp(optarg, "decode") == 0  || strcmp(optarg, "d") == 0)
		return MODE_DECODE_ONLY;
	else
		return MODE_UNDEFINED;
}

struct options * checkParameters(int argc, char **argv) {
	if (argc < 2) {
		erreur("Nombre d'arguments insuffisant\n");
		return NULL; // Pour pas trigger le warning mais de toute façon on y arrivera jamais car erreur va exit(1);
	}

	struct options *options = (struct options *)calloc(1, sizeof(struct options));
	if (access(argv[1], F_OK) == -1) erreur("Fichier d'entree a convetir invalide ou manque de permissions\n");
	options->fileIn = argv[1];
	
	if (argc > 2) {
		int opt;
		while ((opt = getopt(argc, argv, "bm:")) != -1) {
			switch (opt) {
				case 'b': {
					options->binOut = 1;
					break;
				}
				case 'm': {
					int mode = getModeFromArgument(optarg);
					if (mode == MODE_UNDEFINED) {
						// Il est deja a 0 donc pas besoin de l'update
						fprintf(stderr, "%s", "Mode invalide choisi, il a ete remplace par le mode \"all\"\n");
					} else {
						options->mode = mode;
					}
					break;
				}
				default: {
					fprintf(stderr, "Usage: %s file_to_convert [-b] [-m mode]\n", argv[0]);
					exit(1);
					break;
				}
			}
		}
		if (options->mode == MODE_ENCODE_ONLY)
			options->binOut = 1; // Car c'est le meme comportement que -b, on le set pour ne pas s'emebeter a check les deux
	}
	return options;
}

void afficherDico(dico_t *dico) {
	printf("Nombre de mots : %d\n", dico->nb_mots);
	for (int i = 0; i < dico->nb_mots; i++) {
		printf("Mot %d : \"%s\"\n", i+256, dico->mots[i]);
	}
}

void resetDico(dico_t *dico) {
	for (int i = 0; i < dico->nb_mots; i++) {
		free(dico->mots[i]);
	}
	dico->nb_mots = 0;
}

int getCode(char *S, dico_t *dico) {
	if (strlen(S) == 1) { // C'est un seul
		return S[0];
	} else { // C'est forcement un mot du dictionnaire
		int indRecherche;
		for (indRecherche = 0; indRecherche < dico->nb_mots && strcmp(S, dico->mots[indRecherche]) != 0; indRecherche++);
		return 256 + indRecherche;
	}
}

void erreur(char *message) {
	fprintf(stderr, "%s", message);
	exit(1);
}

void initLZW(struct options *options)
{
	int *pfd = (int*) calloc(2, sizeof(int));
	options->pfd = pfd;
	if (pipe(options->pfd) == -1) erreur("Impossible de creer le pipe\n");
	switch (fork()) {
		case -1: {
			erreur("Impossible de creer le processus fils\n");
		}
		case 0: {
			client(options); // Le client lit du pipe
		}
	}
	server(options);
}

void sizeFileComparaison(struct options *options)
{
	struct stat stTxt, stBin;
	stat(options->fileIn, &stTxt);
	stat("res/encoded.bin", &stBin);
	printf("Comparaison de taille entre les 2 fichiers :\n");
	printf("Taille du fichier de base : %.2lfKo\n", stTxt.st_size / 1000.0);
	printf("Taille du fichier compresse a l'aide de l'algorithme : %.2lfKo\n", stBin.st_size / 1000.0);
	if (stTxt.st_size > stBin.st_size) { // Gain de taille -> grands fichiers
		printf("Soit un reduction de %.2lfKo equivalent a un gain de place de %.2lf%%\n", (stTxt.st_size - stBin.st_size) / 1000.0, 100 - (stBin.st_size / (double)stTxt.st_size) * 100);
	} else { // Perte de taille -> petits fichiers
		printf("Soit une perte de %.2lfKo equivalent a un perte de place de %.2lf%%\n", (stBin.st_size - stTxt.st_size) / 1000.0, 100 - (stTxt.st_size / (double)stBin.st_size) * 100);
	}
}

void compareEndResults(struct options *options)
{
	int status;
	switch (fork()) {
		case -1: {
			erreur("Impossible de creer le processus fils\n");
		}
		case 0: {
			execlp("cmp", "cmp", options->fileIn, "res/decoded.txt", NULL);
		}
	}
	if (wait(&status) == -1) erreur("wait");
	printf("Le programme a compare a le fichier de base au fichier ayant passe la phase de decodage grace a la commande :\n\tcmp %s res/decoded.txt\n", options->fileIn);
	if (status == 0) {
		printf("Aucune difference n'a ete observee, le travail a bien ete realise.\n");
	} else {
		printf("Erreur, une difference a ete observee entre les 2 fichiers.\n");
	}
}
