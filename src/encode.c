#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "encode.h"
#include "global.h"
#include "unistd.h"

static dico_t dico;

FILE *fileOut;
struct options *goptions;

void coder_couple(int couple[2]) {
	for (int i = 0; i < 2; i++) { // NOTE: Qui se souvent de nos super amis les multi-char ? C'est de l'histoire ancienne avec cet offset. Wow. Incroyable. Les mots m'en manquent. La joie m'envahit.
		if (couple[i] < 0) {
			couple[i] += 256;
		}
	}
	byte_t oc1, oc2, oc3;
	// Partie basse du nombre 1 dans la partie haute de oc2
	oc2 = couple[0] & 15;
	oc2 <<= 4;
	// Decalage du nombre 1
	couple[0] >>= 4;
	// Ecriture de la partie restante du nombre 1 dans oc1
	oc1 = couple[0] & 255;
	// Ecriture de la partie basse du nombre 2 dans oc3
	oc3 = couple[1] & 255;
	// Decalage du nombre 2
	couple[1] >>= 8;
	// Partie haute du nombre 2 dans la partie basse de oc2
	oc2 = oc2 | (couple[1] & 15);
	if (goptions->binOut == 1) {
		fwrite(&oc1, 1, 1, fileOut);
		fwrite(&oc2, 1, 1, fileOut);
		fwrite(&oc3, 1, 1, fileOut);
	}
	if (goptions->mode == MODE_ALL) {
		if (write(goptions->pfd[1], &oc1, sizeof(byte_t)) == -1) erreur("write oc1");
		if (write(goptions->pfd[1], &oc2, sizeof(byte_t)) == -1) erreur("write oc2");
		if (write(goptions->pfd[1], &oc3, sizeof(byte_t)) == -1) erreur("write oc3");
	}
}

void emettre(const int code) {
	static int cpt = 0;
	static int couple[2];
	cpt += 1;
	if (cpt == 2) {
		cpt = 0;
		couple[1] = code;
		coder_couple(couple);
	} else {
		couple[0] = code;
	}
}

void encoder(struct options *options) {
	goptions = options;
	if (options->pfd != NULL && close(options->pfd[0]) == -1) erreur("Impossible de fermer le pipe");

	char line[BUFSIZ];
	FILE *fileIn = fopen(options->fileIn, "r");
	// On a deja verifie que le fichier existe lors de la verification des parametres

	if (options->binOut == 1) {
		fileOut = fopen("res/encoded.bin", "wb");
		if (fileOut == NULL) erreur("Impossible de creer le fichier bin de sortie, manque de permissions\n");
	}

	int lgth, indRecherche;
	dico.nb_mots = 0;
	char S[BUFSIZ];
	S[0] = '\0';
	char m;

	while (fgets(line, sizeof(line), fileIn)) {
		for (int i = 0; i < strlen(line); i++) {
			m = line[i]; // Recuperer le indRecherche eme carcatere du message
			// Construction de S+m
			lgth = strlen(S);
			S[lgth] = m;
			S[lgth+1] = '\0';
			// Recherche de S+m dans le DICO
			for (indRecherche = 0; indRecherche < dico.nb_mots && strcmp(S, dico.mots[indRecherche]) != 0; indRecherche++);
			// Resultat
			if (indRecherche == dico.nb_mots && lgth != 0) { // Mot pas dans le dico et pas caractere ascii
				dico.mots[dico.nb_mots] = (char *)malloc((lgth + 2) * sizeof(char)); // + 2 pour avoir la place nécessaire
				strcpy(dico.mots[dico.nb_mots], S);
				S[lgth] = '\0'; // On enleve m
				if (strlen(S) > 0) {
					emettre(getCode(S, &dico));
				}
				S[0] = m; // On reinitialise avec m
				S[1] = '\0';
				dico.nb_mots++;
				if (dico.nb_mots == MAX_DICT_SIZE) { // WARNING: Ici on perd une allocation de string au dessus
					resetDico(&dico);
				}
			}
		}
	}
	emettre(getCode(S, &dico));
	emettre(0);
	emettre(0);
	/* Caractere NULL emis deux fois pour pousser le dernier caractere potentiellement bloque et envoyer au moins une fois null pour stopper le decodage
	 * => Resolution du probleme de bloquage mais par contre rend impossible une nouvelle reutilisation propre d'un rappel direct a l'encodage
	 * On se retrouve avec un ajout du dernier caractere + NULL dans le dictionnaire du decodeur ... mais ce n'est pas tres grave en realite
	 */
	resetDico(&dico);
	if (options->pfd != NULL && close(options->pfd[1]) == -1) erreur("Impossible de fermer le pipe");
	fclose(fileIn);
	if (options->binOut == 1) fclose(fileOut);
}