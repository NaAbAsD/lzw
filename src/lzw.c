#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"
#include "global.h"
#include "encode.h"
#include "decode.h"
#include "tools.h"

// TODO: Faire attention que le decode se fasse bien sur un fichier bin.

/*
 * Belle commande valgrind afin de verifier l'etat de notre memoire.
 * valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes --verbose --log-file=valgrind-out.txt ./lzw [fileIn]
 * A noter : fileIn est notre fichier a compresser
 */

int main(int argc, char *argv[]) {
	struct options *options = checkParameters(argc, argv);
	
	if (options->mode == MODE_ALL)
		initLZW(options);
	else if (options->mode == MODE_ENCODE_ONLY)
		encoder(options);
	else
		decoder(options);
	
	if (options->binOut)
		sizeFileComparaison(options);
	if (options->mode == MODE_DECODE_ONLY || options->mode == MODE_ALL)
		compareEndResults(options);
	
	free(options->pfd);
	free(options);
	return 0;
}
