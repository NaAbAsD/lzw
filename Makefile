# Out name
PROG = lzw

# Directories
IDIR = include
ODIR = obj
SDIR = src
RESDIR = res

# Compiler commands
CC = gcc
CFLAGS = -g -Wall -std=c99 -I$(IDIR)

_DEPS = byte.h global.h encode.h decode.h tools.h client.h server.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ= byte.o lzw.o encode.o decode.o tools.o client.o server.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

$(PROG) : $(OBJ)
	$(CC) $(CFLAGS) -o $@ $^

$(ODIR)/%.o : $(SDIR)/%.c $(DEPS)
	$(CC) $(CFLAGS) -c -o $@ $<

# Make commands
.PHONY: run all clean delete folders

folders :
	@mkdir -p $(RESDIR)
	@mkdir -p $(ODIR)

all : folders \
	$(PROG)

clean :
	rm -f $(ODIR)/*.o
	rm -f valgrind-out.txt

delete : clean
	rm -f $(PROG)
	rm -f -r $(RESDIR)
	rm -f -r $(ODIR)
