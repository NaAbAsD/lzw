#ifndef _GLOBAL_
#define _GLOBAL_

#define MAX_DICT_SIZE 3840

/**
 * @brief Definition du type byte_t
 */
typedef unsigned char byte_t;

/**
 * @brief Constantes de byte_t
 */
static const byte_t Z = 0b00000000;
static const byte_t U = 0b00000001;

/**
 * @brief Type dictionnaire
 */
typedef struct {
	char *mots[MAX_DICT_SIZE]; // Sans la table ASCII
	int nb_mots;
} dico_t;

#endif
