#ifndef _TOOLS_
#define _TOOLS_

#define MODE_UNDEFINED -1
#define MODE_ALL 0
#define MODE_ENCODE_ONLY 1 // On peut noter que ce mode a exactement le meme resultat que le mode -b
#define MODE_DECODE_ONLY 2

#include "global.h"

struct options {
	char *fileIn;
	int binOut;
	int mode;
	int *pfd;
};

/**
 * @brief Recupere le mode depuis l'argument -m
 */
int getModeFromArgument(char *optarg);

/**
 * @brief Verifie la presence et la veracite des parametres d'entree
 */
struct options * checkParameters(int argc, char **argv);

/**
 * @brief Affiche le dico
 */
void afficherDico(dico_t *dico);

/**
 * @brief Libere la memoire du dico et le reinitialise pour continuer la compression/decompression
 */
void resetDico(dico_t *dico);

/**
 * @brief Retourne le code de la table ASCII ou du dico
 */
int getCode(char *S, dico_t *dico);

/**
 * @brief Envoie un message d'erreur et quitte avec le code 1
 */
void erreur(char *message);

/**
 * @brief Realise le comportement du lzw de base avec un pere qui encode et un fils qui decode
 */
void initLZW(struct options *options);

/**
 * @brief Compare la taille des deux fichiers (bin et txt)
 */
void sizeFileComparaison(struct options *options);

/**
 * @brief Compare le resultat entre le fichier decode et le fichier de base
 */
void compareEndResults(struct options* options);

#endif