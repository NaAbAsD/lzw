#ifndef _SERVER_
#define _SERVER_

#include "tools.h"

/**
 * @brief Fonction principale du processus pere
 */
void server(struct options *options);

#endif
