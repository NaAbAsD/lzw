#ifndef _DECODE_
#define _DECODE_

#include "global.h"
#include "tools.h"

/**
 * @brief Libere la memoire et termine le processus fils
 */
void finMessage(int *couple);

/**
 * @brief Converti 3 bytes_t en 2 entiers
 */
int * decoder_triplet(byte_t triplet[3]);

/**
 * @brief Converti 2 entiers dans leur caracteres d'origine, le tout en fabriquant le dictionnaire
 */
void decoder_couple(int couple[2]);

/**
 * @brief Fonction fabriquant les triplets a decoder
 */
void recevoir(const byte_t oc);

/**
 * @brief Fonction lisant le fichier a decoder
 */
void decoder(struct options *options);

#endif