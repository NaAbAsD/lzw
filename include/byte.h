#ifndef _BYTE_
#define _BYTE_

#include "global.h"

/**
 * @brief Affiche un octet au format binaire
 * Si le mode = 1, on affiche sa conversion en entier
 */
void bprintf(const byte_t Oc, const char *ident, const int mode);

/**
 * @brief Lit un octet
 */
byte_t bread();

/**
 * @brief Extrait le n eme bit de l'octet
 */
int bit_read(byte_t Oc, int n);

/**
 * @brief Modifie le n eme bit de l'octet
 */
byte_t bit_write(byte_t Oc, int n);

/**
 * @brief Extrait les 4 bits de poids de faible de l'octet
 */
byte_t lire_quartetInf(byte_t O);

/**
 * @brief Extrait les 4 bits de poids de fort de l'octet
 */
byte_t lire_quartetSup(byte_t O);

#endif
