#ifndef _ENCODE_
#define _ENCODE_

#include "tools.h"

/**
 * @brief Code le couple en 3 byte_t qui sont ensuite stockes dans un fichier
 */
void coder_couple(int couple[2]);

/**
 * @brief Fonction fabriquant les couples d'entiers sur 12 bits
 */
void emettre(const int code);

/**
 * @brief Fonction qui genere le dictionnaire
 */
void encoder(struct options *options);

#endif