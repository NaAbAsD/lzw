#ifndef _CLIENT_
#define _CLIENT_

#include "tools.h"

/**
 * @brief Fonction principale du processus fils
 */
void client(struct options *options);

#endif
