# Compression de Lempel-Ziv-Welch 12 bits

## Table des matières

- [A propos](#about)
- [Bien démarrer](#getting_started)
- [Utilisation](#usage)
- [Vérification du résultat](#validate)
- [Fonctionnement basique interne du programme](#how)

## A propos <a name = "about"></a>

Projet de compression sur le principe Lempel-Ziv-Welch (LZW) réalisé lors de l'UE "Compression des données" en 3e année de licence d'informatique.

## Bien démarrer <a name = "getting_started"></a>

Ces instructions vous feront mettre une copie du projet fonctionnelle sur votre machine personnelle pour le développement et à des fins de test.
Ce programme fonctionne uniquement sur un système UNIX - Linux.

### Prérequis

Les différents programmes que vous devez installer afin de compiler le programme.

```
sudo apt install gcc
sudo apt install make
```

Afin de compiler le programme, différentes fonctions sont disponibles. Elles sont applicables à la racine du dossier.

<ol>
    <li><code>make folders</code><br/>
        Crée les dossiers nécessaires à la compilation.
    </li>
    <li><code>make clean</code><br/>
        Supprime les fichiers objets créés lors de la compilation.
    </li>
    <li><code>make delete</code><br/>
        Supprime tous les fichiers liés à la compilation et à l'exécution du programme.
    </li>
    <li><code>make all</code><br/>
        Compile le projet en créant les différents dossiers nécessaires à la compilation, puis les objets de compilation, ainsi que l'exécutable.
    </li>
    <li><code>make run</code><br/>
        Réalise le comportement de <b>make all</b> avec lancement automatique du programme avec un fichier de test.
    </li>
</ol>

## Utilisation <a name = "usage"></a>

Après compilation, il suffit d'exécuter la commande suivante à la racine :

```
    ./lzw chemin_vers_le_fichier_a_compresser
```

Le résultat se trouvera ensuite dans `res/decoded.txt`

Il y a différents types de paramètres d'entrée afin de modifier le fonctionnement du programme :

<ol name = "modes">
    <li><code>-m</code><br/>
        Modifie le mode de fonctionnement du programme :
        <ol style="list-style-type: circle;">
            <li><code>a, all</code></li>
                Réalise le comportement de "base" du programme, c'est à dire :
                Lire un fichier texte puis le compresser.
                Passer l'ensemble des codes à un processus fils à l'aide d'un pipe anonyme.
                Decompresser le texte et stocker le nouveau texte.
            <li><code>e, encode</code></li>
                Réalise uniquement l'encodage d'un fichier texte donné en entrée.
            <li><code>d, decode</code></li>
                Réalise uniquement le décodage d'un fichier binaire donnée en entrée.
        </ol>
    </li>
    <li><code>-b</code><br/>
        Dans le cas ou le programme est dans le mode "all" ou "encode", le programme produira également le fichier binaire correspondant au fichier de base. Autrement le paramètre sera simplement ignoré.
    </li>
</ol>

## Vérification du résultat <a name = "validate"></a>

Dans les cas où le programme aura à décoder notre entrée, le programme réalisera automatiquement une comparaison entre les 2 fichiers à l'aide de la commande cmp fournie sur UNIX - Linux.

Si vous n'êtes pas convaincu du résultat, il suffit d'exécuter la commande suivante :

```
    cmp chemin_du_fichier_de_base res/decoded.txt
```

La fonction ne retourne rien si les deux fichiers sont identiques. Autrement, elle retourne l'emplacement de la première différence entre les deux fichiers.

## Fonctionnement basique interne du programme <a name = "how"></a>

Le fonctionnement du programme dépend des paramètres choisis (cf. <a href="#modes">Modes de fonctionnement</a>).

Lors du choix du mode encodage, le fichier source est lu ligne par ligne et chaque caratère passe à travers l'algorithme d'encodage lzw ou il sera converti en code du dictionnaire. Les codes du dictionnaires sont alors couplés deux à deux et transformés en trois octets avant d'être stockés dans res/encoded.bin.

Lors du choix du mode décodage, le fichier source supposé binaire est lu trois octets par trois. Chaque trio est converti en deux entiers qui sont alors reconvertis en suivant le dictionnaire que nous reconstruisons en même temps. Une fois le texte retrouvé, nous stockons le résultat dans res/decoded.txt.
C'est donc totalement l'opposé de l'encodage.

Lors du choix du mode complet, nous réalisons les deux principes explicités ci-dessus dans l'ordre codage-décodage. La différence majeure de cette méthode est que cette méthode ne crééra pas de fichier bin pour y stocker son encodage. En effet, une fois encodés, les codes sont simplement envoyés par pipe au processus fils qui va décoder directement ces codes. Pour obtenir le fichier binaire il s'agira d'ajouter l'option -b pour également le créer malgré le passage par pipe.


